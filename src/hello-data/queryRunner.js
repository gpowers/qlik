const WebSocket = require('ws');
const enigma = require('enigma.js');
const schema = require('enigma.js/schemas/3.2.json');

async function createApp() {
  const session = enigma.create({
    schema,
    url: "ws://localhost:19076/app/",
    createSocket: url => new WebSocket(url),
  });

  const qix = await session.open();
  const app = await qix.createSessionApp();

  await app.createConnection({
    qName: "data",
    qConnectionString: "/data/",
    qType: "folder",
  });

  return { app, session };
}

// returns a data layout?
async function runQuery(script, queryProperties) {
  try {
    const { app, session } = await createApp();

    await app.setScript(script);
    await app.doReload();
    const object = await app.createSessionObject(queryProperties);
    const layout = await object.getLayout();
    await session.close();

    return layout;
  } catch(e) {
    console.error(e);
    process.exit(1);
  }
}

module.exports = runQuery;